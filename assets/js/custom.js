/**
 * ------------------------------------------
 * APPINALL POPUP WIGHET FOR LIVECHAT BOT
 * ------------------------------------------
 * @author: codeterium <codeterium@gmail.com>
 * ------------------------------------------
 * Version: 3.0.1
 * ------------------------------------------
 * <div id="appinall_widget"></div><script src='https://appinall.com/widget/appinall/appinall.widget.js'></script>
 * <iframe src="https://appinall.com/widget/bot/?name=MJ_MD_for_Leafly" width="100%" height="400"></iframe>
 */

 
(function () {

	// Polyfills
	if (typeof Array.prototype.forEach != 'function') {
		Array.prototype.forEach = function(callback){
		  for (var i = 0; i < this.length; i++){
			callback.apply(this, [this[i], i, this]);
		  }
		};
	}

	/**
	 * Main Class
	 * @param {string} name 
	 */
	var AppinallClass = function(name){
		
		this.name 		= name;
		this.src 		= '';
		this.isMobile 	= false; // Detect mobile
		this.isShow 	= false; // Detect mobile

		// Dynamic elements
		this.page 			= null; // Root container to load all content
		this.parent 		= null; // Root container to load all content
		this.parent_style 	= null; // Styles for Root container

		// Meta Viewport
		this.viewportElement = null;
		this.viewportDefault = null;
		this.viewportNew = 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover';


		/**
		 * @var content
		 */
		this.content ={
			holder: {
				node	: null,
				style	: null,
			},
			iframe:	{
				node	: null,
				style	: null,
			},

		}

		/**
		 * @var button
		 */
		this.button = {
			button: {
				node	: null,
				style	: null,
			},
			closer:	{
				node	: null,
				style	: null,
			},
			opener:	{
				node	: null,
				style	: null,
			},
		};


		this.getMeta =  function(metaName){
			
			var metas = document.getElementsByTagName('meta');
		  
			for (var i = 0; i < metas.length; i++) {
			  if (metas[i].getAttribute('name') === metaName) {
				return metas[i];
			  }
			}
		  
			return '';
			
		}

		/**
		 * Initialization function
		 */
		this.init = function(){
			
			// Get default viewport value
			this.viewportElement = this.getMeta('viewport');
			if(this.viewportElement){
				this.viewportDefault = this.viewportElement.getAttribute('content');
				if(this.viewportDefault){
					this.log(this.viewportDefault);
				}
			}

			
		}

		/**
		 * Assing Stales for Elements
		 */
		this.assignStyles = function(){

			var flex_style;
			var transition_style;

				flex_style += 'display: -webkit-box;display: -webkit-flex;display: -ms-flexbox;display: flex;';
				flex_style += '-webkit-flex-wrap: nowrap;-ms-flex-wrap: nowrap;flex-wrap: nowrap;';
				flex_style += '-webkit-box-align: center;-webkit-align-items: center;-ms-flex-align: center;align-items: center;';
				flex_style += '-webkit-align-content: center;-ms-flex-line-pack: center;align-content: center;-webkit-box-pack: center;';
				flex_style += '-webkit-justify-content: center;-ms-flex-pack: center;justify-content: center; ';

				transition_style += '-webkit-transition: opacity  1.2s linear, scale 1.8s linear, width 0.21s, height 0.21s , -webkit-transform 1.2s linear;';
				transition_style += 'transition: opacity  1.2s linear, scale 1.8s linear, width 0.21s, height 0.21s , -webkit-transform 1.2s linear;';
				transition_style += '-o-transition: transform 1.2s linear , opacity  1.2s linear, scale 1.8s linear, width 0.21s, height 0.21s;';
				transition_style += 'transition: transform 1.2s linear , opacity  1.2s linear, scale 1.8s linear, width 0.21s, height 0.21s;';
				transition_style += 'transition: transform 1.2s linear , opacity  1.2s linear, scale 1.8s linear, width 0.21s, height 0.21s , -webkit-transform 1.2s linear; ';

				transition_style += '-webkit-animation: 250ms ease 0s 1 normal none running animation-bhegco;';
				transition_style += 'animation: 250ms ease 0s 1 normal none running animation-bhegco; ';


  

			this.parent_style += 'width: 376px;height: -webkit-calc(100% - 120px);height: calc(100% - 120px);max-height: 804px;min-height: 250px; ';
			this.parent_style += 'position:fixed; z-index:5147483000;';
			this.parent_style += 'padding:0px; margin:0px;';
			this.parent_style += 'right:15px; bottom:0;';


			this.button.button.style += flex_style + transition_style;
			this.button.button.style += 'font-weight:700; color:white; font-size:1em;';
			this.button.button.style += 'display:block;z-index: 2147483000; position: absolute; bottom: 16px;right: 0px; left:0;margin: 0 auto;';
			this.button.button.style += 'width: 90%; height: 40px;-webkit-border-radius: 40px;border-radius: 40px;';
			this.button.button.style += 'background: #4865FF none repeat scroll 0% 0%; ';

			this.button.button.style += '-webkit-box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 6px 0px, rgba(0, 0, 0, 0.16) 0px 2px 32px 0px;';
			this.button.button.style += 'box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 6px 0px, rgba(0, 0, 0, 0.16) 0px 2px 32px 0px;';
			this.button.button.style += 'cursor:pointer;' + flex_style;

			this.button.opener.style +=	flex_style + transition_style;
			this.button.opener.style +=	'width: 100%;height: 40px;max-width: 100%;max-height: 100%;';
			this.button.opener.style +=	'margin:0px; padding:0px;';

			this.button.closer.style +=	flex_style + transition_style;
			this.button.closer.style +=	'width: 40px;height: 50px;max-width: 100%;max-height: 100%;';
			this.button.closer.style +=	'margin:0px; padding:0px;';
			this.button.closer.style +=	'fill: white; visibility:collapse; opacity:0;';


			this.content.holder.style += 'width: 100%;height: 100%;max-width: 100%;max-height: 100%;-webkit-border-radius: 5px;border-radius: 5px; ';
			this.content.holder.style += 'display: block; visibility:collapse; position: relative ;z-index: 100;';
			this.content.holder.style += 'background: white;-webkit-box-shadow: 0 0 10px 1px rgba(0, 0, 0, 0.3);box-shadow: 0 0 10px 1px rgba(0, 0, 0, 0.3);';
			this.content.holder.style += 'padding: 0px;margin: 0px auto;';
			//this.content.holder.style += 'bottom: 0px; left: 0px; width: calc(100% - 2px); height: 0;';
			this.content.holder.style += '-webkit-transition: all 0.2s linear;-o-transition: all 0.2s linear;transition: all 0.2s linear; ';	

			if (navigator.userAgent.match(/(iPod|iPhone|iPad)/i) || Math.abs(window.orientation) === 90) {
				this.content.holder.style += '-webkit-overflow-scrolling: touch !important;overflow-y: scroll !important;';
				this.content.holder.style += 'overflow-x:hidden;overflow-y:hidden;';
	
			}		
			this.content.iframe.style += 'width: 100%;  height: 100%;  max-width: 100%;  max-height: 100%;  padding: 0;  margin: 0;  overflow: hidden;  -webkit-border-radius: 5px;  border-radius: 5px;';			
			//this.content.iframe.style += 'border: 1px solid rgb(52, 152, 220);';

		}

		/**
		 * Create frame DOM element
		 */
		this.createContent = function(){

			this.content.holder.node	= document.createElement("div");
			this.content.iframe.node	= document.createElement("iframe");

			this.content.holder.node.id	= 'appinall__frame__wrapper';
			this.content.iframe.node.id	= 'appinall__frame__widget';

			this.content.holder.node.style.cssText	= this.content.holder.style;
			this.content.iframe.node.style.cssText	= this.content.iframe.style;
			
			this.content.iframe.node.src = this.src;
			this.content.iframe.node.frameBorder = "0";
			this.content.iframe.node.style.width = "100%";

			this.content.holder.node.appendChild(this.content.iframe.node);	
		}

		/**
		 * Create button DOM element
		 */
		this.createButton = function(){

			var _this = this;

			this.button.button.node	= document.createElement("div");
			this.button.opener.node	= document.createElement("div");
			this.button.closer.node	= document.createElement("div");

			this.button.button.node.id	= 'appinall__button';
			this.button.opener.node.id	= 'appinall__opener';
			this.button.closer.node.id	= 'appinall__closer';

			this.button.button.node.style.cssText	= this.button.button.style;
			this.button.opener.node.style.cssText	= this.button.opener.style;
			this.button.closer.node.style.cssText	= this.button.closer.style;

			this.button.button.node.innerHTML = '';
			this.button.opener.node.innerHTML = 'LIVE CHAT WITH US NOW';
			this.button.closer.node.innerHTML = '<svg width="14" height="14"><path d="M13.978 12.637l-1.341 1.341L6.989 8.33l-5.648 5.648L0 12.637l5.648-5.648L0 1.341 1.341 0l5.648 5.648L12.637 0l1.341 1.341L8.33 6.989l5.648 5.648z" fill-rule="evenodd"></path></svg>';


			this.button.button.node.appendChild(this.button.opener.node);	
			this.button.button.node.appendChild(this.button.closer.node);	


			//this.button.button.node.parent 			= this.parent;
			this.button.button.node.node 			= this.content.holder.node;
			//this.button.button.node.xframe 			= this.content.iframe.node;
			//this.button.button.node.open_element 	= this.button.opener.node;		
			//this.button.button.node.close_element 	= this.button.closer.node;	

			this.button.button.node.onclick = function (event) {

				event.preventDefault();
				_this.detectMobile();

				if (this.node.style.visibility === "collapse") {
					_this.isShow = true;
				} else {
					_this.isShow = false;
				}
				_this.changeState();

			};	


		}

		/**
		 * Log console
		 */
		this.log = function(text){
			console.log(text)
		}


		this.attachWindow = function(){

			this.parent.style.cssText = this.parent_style;			
			this.parent.appendChild(this.content.holder.node);
			this.parent.appendChild(this.button.button.node);			

			//Moving widget code to the end of page
			var bodyElement = null;
			bodyElement = document.querySelector('body');
			if(bodyElement){
				this.parent.remove();
				bodyElement.appendChild(this.parent);
			}
		}

		/**
		 * Change elements state
		 */
		this.changeState = function(){

			if(this.isMobile) {
				this.log('Mobile device');
				//this.parent.style.position = 'absolute';
				this.parent.style.width 	= '100%';
				this.parent.style.height 	= '100%';
				this.parent.style.right 	= '0';
			} else {
				this.log('Desktop device');
				//this.parent.style.position = 'relative';
				this.parent.style.width 	= '376px';
				this.parent.style.height 	= 'calc(100% - 120px);';
				this.parent.style.right 	= '15px';
				this.parent.style.background = 'transparent';
				this.page.style.overflow = 'inherit';
			}


			if (this.isShow) {

				this.button.button.node.style.width 		= '40px';
				this.button.button.node.style.height 		= '40px';
				this.button.button.node.style.right 		= '8px';
				
				this.button.opener.node.style.visibility 	= 'collapse';
				this.button.opener.node.style.opacity 		= '0';
				this.button.opener.node.style.transform 	= 'scale(0);rotate(180deg);';
				this.button.opener.node.style.width 		= '0';

				this.button.closer.node.style.visibility 	= 'visible';
				this.button.closer.node.style.opacity 		= '1';
				this.button.closer.node.style.transform 	= 'rotate(0deg)';
				this.button.closer.node.style.width 		= '100%';

				this.content.holder.node.style.visibility 	= "visible";
				this.content.holder.node.style.opacity 		= "1";
				this.content.holder.node.style.height		= "calc(100% - 76px)" ;

				if(this.isMobile){
					this.parent.style.background = 'white';
					this.page.style.overflow = 'hidden';
				};

				

				this.content.holder.node.focus();
				this.parent.scrollIntoView();

				this.viewportElement.setAttribute("content", this.viewportNew);
			} else {

				this.button.button.node.style.width 		= '90%';
				this.button.button.node.style.height 		= '40px';
				this.button.button.node.style.right 		= '0px';

				this.button.opener.node.style.visibility 	= 'visible';
				this.button.opener.node.style.opacity 		= '1';
				this.button.opener.node.style.transform 	= 'scale(1);rotate(0deg);';
				this.button.opener.node.style.width 		= '100%';

				this.button.closer.node.style.visibility 	= 'collapse';
				this.button.closer.node.style.opacity 		= '0';
				this.button.closer.node.style.transform 	= 'rotate(180deg)';
				this.button.closer.node.style.width			= '0';

				this.content.holder.node.style.visibility 	= "collapse";
				this.content.holder.node.style.opacity 		= "0";
				this.content.holder.node.style.height		= "0px" ;
				this.content.holder.node.style.marginTop 	= "0px";

				

				this.parent.style.background = 'transparent';
				this.page.style.overflow = 'inherit';

				this.viewportElement.setAttribute("content", this.viewportDefault);

			}


		}


	}


	AppinallClass.prototype.detectMobile = function() {
		
		/*
		var check = false;
		(function(a)
		{
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;
		})(navigator.userAgent||navigator.vendor||window.opera);
		this.isMobile = check;
		this.log(check);
		return check;		
		*/

		if(window.innerWidth > window.innerHeight){ // if Portrait

			if(window.innerHeight <= 480 && window.innerWidth <= 800){
				this.isMobile = true;
			} else {
				this.isMobile = false;
			}

		} else { // if Landscape

			if(window.innerWidth <= 600){
				this.isMobile = true;
			} else {
				this.isMobile = false;
			}
		}

		this.changeState();
		return this.isMobile;
	}

	AppinallClass.prototype.doWidget = function(){
		this.init();
		this.assignStyles();
		this.createContent();
		this.createButton();
		this.attachWindow();
		this.detectMobile();
	}

	



	/**
	 * DOM ready event listener
	 */
	var ready = (function () {
		var ready_event_fired = false;
		var ready_event_listener = function (fn) {

			// Create an idempotent version of the 'fn' function
			var idempotent_fn = function () {
				if (ready_event_fired) {
					return;
				}
				ready_event_fired = true;
				return fn();
			}

			// The DOM ready check for Internet Explorer
			var do_scroll_check = function () {
				if (ready_event_fired) {
					return;
				}

				// If IE is used, use the trick by Diego Perini
				// http://javascript.nwbox.com/IEContentLoaded/
				try {
					document.documentElement.doScroll('left');
				} catch(e) {
					setTimeout(do_scroll_check, 1);
					return;
				}

				// Execute any waiting functions
				return idempotent_fn();
			}

			// If the browser ready event has already occured
			if (document.readyState === "complete") {
				return idempotent_fn()
			}

			// Mozilla, Opera and webkit nightlies currently support this event
			if (document.addEventListener) {

				// Use the handy event callback
				document.addEventListener("DOMContentLoaded", idempotent_fn, false);

				// A fallback to window.onload, that will always work
				window.addEventListener("load", idempotent_fn, false);

			// If IE event model is used
			} else if (document.attachEvent) {

				// Ensure firing before onload; maybe late but safe also for iframes
				document.attachEvent("onreadystatechange", idempotent_fn);

				// A fallback to window.onload, that will always work
				window.attachEvent("onload", idempotent_fn);

				// If IE and not a frame:
				// continually check to see if the document is ready
				var toplevel = false;

				try {
					toplevel = window.frameElement == null;
				} catch (e) {}

				if (document.documentElement.doScroll && toplevel) {
					return do_scroll_check();
				}
			}
		};
		return ready_event_listener;
	})();

	/**
	 * Run on ready state
	 */
	var ready_app = function () {

		/*
		function addSC(text){
			var newScript = document.createElement('script');
			newScript.type = 'text/javascript';
			newScript.src = text;
			//document.getElementsByTagName('body')[0].appendChild(newScript);
			document.getElementById('appinall_widget').appendChild(newScript);
		}
		

		addSC("https://appinall.com/widget/bot/runtime.js");
		addSC("https://appinall.com/widget/bot/polyfills.js");
		addSC("https://appinall.com/widget/bot/styles.js");
		addSC("https://appinall.com/widget/bot/vendor.js");
		addSC("https://appinall.com/widget/bot/main.js");
		/*
		function load(script) {
			//document.write('<'+'script src="'+script+'" type="text/javascript"><' + '/script>');
		}

		load("https://appinall.com/widget/bot/runtime.js");
		load("https://appinall.com/widget/bot/polyfills.js");
		load("https://appinall.com/widget/bot/styles.js");
		load("https://appinall.com/widget/bot/vendor.js");
		load("https://appinall.com/widget/bot/main.js");
		*/

		var Appinall = new AppinallClass('Appinall');

		if(Appinall)
		{
			Appinall.parent = document.getElementById('appinall_widget');
			Appinall.page = document.querySelector('body');
			Appinall.src = 'https://appinall.com/widget/bot/?name=MJ_MD_for_Leafly';
			//Appinall.src = '';
			Appinall.doWidget();


			window.addEventListener('resize', function(event){
				Appinall.detectMobile();
			});
		}


	};

	/**
	 * Pass functions to ready()
	 */
	ready(function () {
		ready_app();
	});

})();
