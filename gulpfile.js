/**
 * ------------------------------------------
 * APPINALL POPUP WIGHET FOR LIVECHAT BOT
 * ------------------------------------------
 * @author: codeterium <codeterium@gmail.com>
 * ------------------------------------------
 * Version: 3.0.1
 * ------------------------------------------
 * 
 */

"use strict";

/**
 *  Required Plugins
 */
var gulp = require('gulp');
var rigger = require('gulp-rigger'); 	//templater
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint'); 	// npm install --save-dev jshint gulp-jshint
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var changed = require('gulp-changed');

/**
 * Path variables
 */
var src = {
	js: './assets/js/main.js',
};
var dest = {
	js: './_poduction/assets/js',
};
var watch = {
	js: './assets/js/**/*.js',
};

/**
 * Plugins options
 */
var options = {
	jshint: '',
	jshint_reporter: 'default',
	uglify: { mangle: false },
	clean: { read: false }
};

/**
 * Display errors
 * @param {*} err 
 */
function handleError(err) {
	console.log(err.toString());
	this.emit('end');
}

/**
 * Javascript from source to production
 */
gulp.task('js', function () {

	console.log('Start JS routine');

	return gulp.src(src.js)
		.pipe(plumber())
		.pipe(rigger())
		//.pipe( changed( dest.js ) )
		.pipe(jshint(options.jshint))
		//.pipe( jshint.reporter( options.jshint_reporter ) )
		.pipe(uglify(options.uglify))
		.pipe(concat('appinall.widget.js'))
		.pipe(plumber.stop())
		.pipe(gulp.dest(dest.js))
		.pipe(notify({ message: 'JS routine complete.' }))
});

/**
 * Clean destionations
 */
gulp.task('clean', function () {

	return gulp.src([dest.js], options.clean)
		.pipe(clean())
		.pipe(notify({ message: 'Clean task complete.' }))
});

/**
 * Build routines
 */
gulp.task('build',
	gulp.series(
		gulp.parallel(
			'js',
		)
	)
);

/**
 * Start wathching
 */
gulp.task('watch', function () {

	console.log('Start watch');
	gulp.watch(watch.js, gulp.series('js'));

});

/**
 * Default task
 */
gulp.task('default',
	gulp.series(
		gulp.parallel(
			'build',
			'watch'
		)
	)
);